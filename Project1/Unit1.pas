unit Unit1;

interface

uses SysUtils, StrUtils, Materials;

type
  TParmeters = Record
    default: Boolean;
    source_image: Boolean;
    path_to_source: String;
    result_image: Boolean;
    path_to_result: String;
    overwrite: Boolean;
    result_format: String;
    result_format_size: Byte
  end;

function GetConsoleApplicationParameters(var AAppParameters: TParmeters; var err: String): Boolean;
function CheckFileExists(APath_to_source: String): Boolean;
function CheckParams(AAppParameters: TParmeters; var err: String): Boolean;
function GenerateNormalMap(AAppParameters: TParmeters; var err: String): Boolean;

implementation

function GetConsoleApplicationParameters(var AAppParameters: TParmeters; var err: String): Boolean;
  var
    i: Byte;
    s: String;
    n: integer;
    b: Boolean;
begin
  //
  try
    Result := False;
    b := True;
    with AAppParameters do begin
      default := True;
      source_image := False;
      path_to_source := '';
      result_image := False;
      path_to_result := '';
      overwrite := False;
      result_format := 'png';
      result_format_size := 1;
    end;
    for i := 1 to ParamCount do begin
      if LoverCase(ParamStr(i)) = '--source_image'
        then begin
          b := False;
          AAppParameters.source_image := True;
          if (LeftStr(ParamStr(i + 1), 2) <> '--')
            then AAppParameters.path_to_source := ParamStr(i + 1);
        end
          else
      if ParamStr(i) = '--result_image'
        then begin
          b := False;
          AAppParameters.result_image := True;
          if (LeftStr(ParamStr(i + 1), 2) <> '--')
            then AAppParameters.path_to_result := ParamStr(i + 1);
        end
          else
      if ParamStr(i) = '--overwrite'
        then begin
          b := False;
          AAppParameters.overwrite := True;
        end
          else
      if ParamStr(i) = '--result_format'
        then begin
          b := False;
          if     (Pos('=', ParamStr(i + 1)) > 0)
             AND (   (ContainsStr(ParamStr(i + 1), 'jpg') = True)
                  OR (ContainsStr(ParamStr(i + 1), '1=') = True)
             )
            then begin
              AAppParameters.result_format := 'jpg';
              s := RightStr(ParamStr(i + 1), Pos('=', ReverseString(ParamStr(i + 1))) - 1);
              if TryStrToInt(s, n) = False
                then n := 90;
              AAppParameters.result_format_size := n;
            end
              else begin
                if (AnsiMatchStr(ParamStr(i + 1), ['1', 'jpg']) = True)
                  then begin
                    AAppParameters.result_format := 'jpg';
                    AAppParameters.result_format_size := 90;
                  end
                    else
                if (AnsiMatchStr(ParamStr(i + 1), ['2', 'bmp']) = True)
                  then begin
                    AAppParameters.result_format := 'bmp';
                    AAppParameters.result_format_size := 1;
                  end;
              end;
        end;
    end;
    AAppParameters.default := b;
    Result := True;
  except
    on E: Exception
      do err := Format('������ ��� ������� ����������: %s', [E.Message]);
  end;
end;

function CheckFileExists(APath_to_source: String): Boolean;
begin
  //
  Result := FileExists(APath_to_source);
end;

function CheckParams(AAppParameters: TParmeters; var err: String): Boolean;
begin
  //
  Result := True;
  err := '';

  if AAppParameters.default = True
    then err := '�������' + Char(10) +
                '--source_image - ���� � ��� ����� ��������� �������� ����������� (������� .jpg, .png, .bmp)' + Char(10) +
                '--result_image - ���� � ��� ����� �������� ����� �������. ���� �������� �� �����, �� ����� ��������� ��� ��, ��� source_image � ����� � ��� �� �����, �� � ����������� .nm' + Char(10) +
                '--overwrite - ���� ������ ������ ��������, �� � ������, ���� result_image ��� ���������� �� �����, �� ����� �����������. � ��������� ������ ������� ������ �� �������.' + Char(10) +
                '--result_format - ������ ��������� �����. ���� �� ������, �� png. ���� ������, �� 1 = jpg, 2 = bmp. � ������ ���� ������ jpg, �� ����� "=" ����� ������� ������� ������ 0-100 (��-��������� 90)' +
                //'--contour - ���� � ����� �������. ������ ����� ���� ����� �� ������. ������� ��� �� ������������ ��������. ���� ������ �� �����, �� ������ ��������� �������������.' + Char(10) +
                //'--sobel_scale � ������� (������) ������� ������. ���� �� �����, �� 1. ���� ����� 0, �� ��� ������������ �� ������� sobel_filter' + Char(10) +
                //'--sobel_filter - ������ ���� ������ � �������� (�����������, ���������� ������ ������� �������). ���� �� �����, �� ������ ������ �� ����������� � �������� �������, � ���� ������ ��������� ������� (rgb=128, 128, 255)' + Char(10) +
                //'--width - ������ ������ � ��' + Char(10) +
                //'--height - ������ ������ � ��' + Char(10) +
                //'--edge_size - ������ ����� � ��. ���� �������� �� �����, �� ����� �� ���������' + Char(10) +
                //'--edge_radius - ������ ����� � ��. ���� ������ �� �����, �� ��-��������� ����� ������ �����' + Char(10) +
                //'--edge-scale - ������� ������� �����. ���� �� �����, �� 1.' +
                '';

  if (err = '') AND (AAppParameters.source_image = False)
    then err := '�� ����� ������������ ��������(--source_image)';

  if (err = '') AND (AAppParameters.path_to_source = '')
    then err := '�� ����� ������������ ��������(���� �� ��������� �����)';

  if (err = '') AND (AnsiMatchStr(ExtractFileExt(AAppParameters.path_to_source), ['.jpg', '.png', '.bmp']) = True)
    then err := '�������� ���� ����� ����������������� ����������(.jpg, .png, .bmp)';

  if (err = '') AND (CheckFileExists(AAppParameters.path_to_source) = False)
    then err := '�� ����� ������������ ��������(�������� ���� �� ����������)';

  Result := err = '';
end;

function GenerateNormalMap(AAppParameters: TParmeters; var err: String): Boolean;
  var
    mt: TMaterialTexture;
    JpgQR: TJPEGQualityRange
begin
  //
  try
    try
      Result := False;

      mt := TMaterialTexture.CreateNamed('result.nm');

      mt.LoadFromFile(AAppParameters.path_to_source)

      mt.GenNormalMap(0, 0, 0, 600, 600, 1, 7, dst: IChangableSampler);
      {
      BorderRadius, BorderWidth, BorderScale, MapWidth, MapHeight,
        Scale: Single; KernelType: Integer; dst: IChangableSampler; NoMips: boolean = false;
          SobelDisabled: Boolean = false; EnableV: boolean = true; EnableH: boolean = true;
          CombinePix: Integer = 1
      }

      if (AAppParameters.result_format = 'jpg')
        then begin
          JpgQR := TJPEGQualityRange.Create();
          mt.GetAsJpg(JpgQR): TJpegImage;

        end
          else
      if (AAppParameters.result_format = 'png')
        then begin
          //function GetAsPng: Graphics.TBitmap;virtual;abstract;
        end
          else
      if (AAppParameters.result_format = 'bmp')
        then begin
          mt.GetAsBmp(): Graphics.TBitmap;

        end;
      Result := True;
    except
      on E: Exception
        do err := Format('������ ��� ��������� ����� ��������: %s', [E.Message]);
    end;
  finally
    mt.Destroy;
  end;
end;

end.
