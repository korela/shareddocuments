program Project2;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Windows,
  Unit1 in 'Unit1.pas',
  Materials in 'Materials.pas';

var
  AppParameters: TParmeters;
  errMsg: String;
  res: Boolean;

begin
  { TODO -oUser -cConsole Main : Insert code here }
  SetConsoleOutputCP(1251);
  res := False;

  res := getConsoleApplicationParameters(AppParameters, errMsg);
  if res = True
    then begin
      res := CheckParams(AppParameters, errMsg);
      if res = True
        then begin
          res := GenerateNormalMap(AppParameters, errMsg);
          if res = True
            then
              else WriteLn(errMsg);
        end
          else WriteLn(errMsg);
    end
      else WriteLn(errMsg);

  if res
    then WriteLn('0')
      else WriteLn('1');
  ReadLn;
end.
