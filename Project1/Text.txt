Тестовое задание «Разработка утилиты «Генератор карт рельефа»
Введение
В компьютерной графике для описания микро рельефа поверхности трехмерного объекта часто используется структура данных, которая называется «Normal map» (карта нормалей). Карта представляет собой двумерный массив векторов нормалей ко всей или части исходной поверхности (https://en.wikipedia.org/wiki/Normal_mapping). Так как для хранения карты используются традиционные графические форматы (jpg, png, …), имеющие три цветовых канала (r, g, b) в диапазоне от 0-255, то для задания координат произвольных единичных векторов используется простое кодирование:
R = 128 + x * 127
G = 128 + y * 127
B = 128 + z * 127, где
x, y, z – координаты вектора нормали в диапазоне [-1..1]
​Данную карту можно получить как сканированием реальной поверхности (например, лазерным дальномером), так и математически - с помощью оператора градиента.
На практике часто пользуются тем фактом, что яркость рассеянного поверхностью света пропорциональна углу между направлением на источник света и локальной нормалью. Таким образом вычислив градиент яркости изображения мы получим неплохое приближение карты рельефа исходной поверхности. Мы можем варьировать размер деталей рельефа, применяя к исходному изображению фильтр размытия. А также варьировать высоту рельефа, применяя масштабный коэффициент к яркости изображения. Для вычисления градиента яркости можно использовать оператор Собеля (https://ru.wikipedia.org/wiki/%D0%9E%D0%BF%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80_%D0%A1%D0%BE%D0%B1%D0%B5%D0%BB%D1%8F)
 
 
Задание
Используя предоставленные готовые реализации фильтра Собеля, вам необходимо создать консольное приложение, которое будет получать карту рельефа (normal map) из цветного изображения по заданным параметрам.
 
Так как мы даем почти весь необходимый для решения задачи код, данное задание не является оплачиваемым. Цель задания – посмотреть на ваш стиль программирования, проверить умение разбираться в чужом коде и вашу внимательность при прочтении ТЗ.
У нас есть гораздо более мощные инструменты для обработки изображений и поэтому нет цели сделать вашими руками полноценный инструмент – это всего лишь тестовое задание. Если вам понравится результат, который вы получите, вы вправе использовать его в своих проектах.
 
 
Вы можете решить задание двумя способами:
1) Быстро
В этом случае будет достаточно, чтобы приложение создавало прямоугольную карту рельефа из исходного изображения (без кромки и ограничивающего контура, см. ниже).
Максимальный срок решения задачи – 1 день
2) Медленно
Срок решения вы определяете сами, как только получите задание.
В этом случае в зависимости от передаваемых параметров, в дополнение к рельефу, полученному из градиента яркости, необходимо добавлять процедурно создаваемый рельеф, имитирующий кромку поверхности. Профиль представляет собой сектор дуги эллипса. Два радиуса эллипса передаются в параметрах приложения.

 
edge_size
edge_radius * edge_scale
 
Также на вход может быть передан ограничивающий контур поверхности. В этом случае процедурно создаваемая кромка должна проходить по указанному контуру.
Так как используются физические размеры кромки и контура в миллиметрах, а цветное изображение задано в пикселях, необходимо определить и физический размер исходного изображения. Это делается через параметры width и height.
 
В общем случае приложение принимает на вход следующие аргументы (для варианта 1 – только часть из них):
• --source_image - путь и имя файла исходного цветного изображения (форматы .jpg, .png, .bmp)
• --result_image - путь и имя файла итоговой карты рельефа. Если параметр не задан, то карта называется так же, как source_image и лежит в той же папке, но с расширением .nm
• --overwrite - если указан данный параметр, то в случае, если result_image уже существует на диске, он будет перезаписан. В противном случае утилита ничего не сделает.
• --result_format - формат выходного файла. Если не указан, то png. Если указан, то 1 = jpg, 2 = bmp. В случае если формат jpg, то через "=" можно указать степень сжатия 0-100 (по-умолчанию 90)
• --contour - путь к файлу контура. Контур может быть задан не всегда. Поэтому это не обязательный параметр. Если контур не задан, то плитка считается прямоугольной.
• --sobel_scale – масштаб (высота) рельефа Собеля. Если не задан, то 1. Если равен 0, то это эквивалентно не заданию sobel_filter
• --sobel_filter - размер ядра Собеля в пикселях (сглаживание, определяет размер деталей рельефа). Если не задан, то фильтр собеля не применяется к исходной тестуре, а весь рельеф считается плоским (rgb=128, 128, 255)
• --width - ширина плитки в мм
• --height - высота плитки в мм
• --edge_size - ширина каймы в мм. Если параметр не задан, то кайма не создается
• --edge_radius - радиус каймы в мм. Если радиус не задан, то по-умолчанию равен ширине каймы
• --edge-scale - масштаб рельефа каймы. Если не задан, то 1.
Результирующее изображение должно быть того же размера, что и исходное цветное.
В консоль должен выводиться текст всех ошибок, а также справка если утилиту запустить без параметров.
Результат работы программы (exit code) должен быть 0 в случае успеха или 1, если ошибка и выходной файл не был создан.
Примеры входных изображений находятся в папке imagesDemo.
Примеры итоговых изображений находятся в папке resultImagesDemo.
Примеры готовых реализаций фильтра Собеля (в том числе с процедурным созданием кромки по контуру) можно посмотреть в следующих модулях (приложены к заданию):
Materials.pas
//процедура позволяет создать карту рельефа с помощью фильтра Собеля и добавить прямоугольную рамку к ней
procedure TMaterialTexture.GenNormalMap
GLMultiPolygon.pas
//процедура генерации рельефа кромки для контура
procedure TGLContours.GenNormalMapEx
